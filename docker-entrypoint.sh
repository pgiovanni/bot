#!/bin/bash

# Generate Bindings
pnpm db:gen

# Deploy Migrations onto Databases
pnpx prisma migrate deploy --schema=prisma/bot-db/schema.prisma
pnpx prisma migrate deploy --schema=prisma/index-db/schema.prisma

# Set proper permissions for the Database
[[ $INDEX_DATABASE =~ [A-Za-z]+://([A-Za-z]+):([^@]+)@([A-Za-z_-]+):[0-9]+\/([A-Za-z_-]+).* ]]
export POSTGRES_USER=${BASH_REMATCH[1]}
export PGPASSWORD=${BASH_REMATCH[2]}
export POSTGRES_HOST=${BASH_REMATCH[3]}
export POSTGRES_DB=${BASH_REMATCH[4]}

if [ -z "$POSTGRES_USER" ]; then
  echo "Extraction RegEx doesn't match! Please check your Database URL!"
  exit 1
fi

psqlexecGRF="psql -U $POSTGRES_USER -d $POSTGRES_DB -h $POSTGRES_HOST -c"
$psqlexecGRF "GRANT USAGE ON SCHEMA public TO $GRAFANA_USER;"
$psqlexecGRF "GRANT SELECT ON ALL TABLES IN SCHEMA public TO $GRAFANA_USER;"

# Start the Bot
pnpm run start
