FROM node:lts-bullseye

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install postgresql-client -y && \
    npm install -g pnpm && \
    mkdir -p /home/node/app/node_modules && \
    chown -R node:node /home/node/app

WORKDIR /home/node/app
COPY package.json pnpm-lock.yaml ./
USER node

RUN pnpm install
COPY --chown=node:node . ./
RUN chmod +x /home/node/app/docker-entrypoint.sh

EXPOSE 9090

CMD ["./docker-entrypoint.sh"]
