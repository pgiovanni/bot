import { TimeBuilder } from '@/lib/builders/time-builder';

describe('Time Builder Tests', () => {
  test('Date Correspondence', () => {
    let date: Date = new Date(2021, 11, 10);
    let builder: TimeBuilder = new TimeBuilder(date).addWeek(-1);
    let expected: Date = new Date(2021, 11, 3);
    expect(builder.toDate().getTime()).toBe(expected.getTime());

    builder.addWeek(-1);
    expected = new Date(2021, 10, 26);
    expect(builder.toDate().getTime()).toBe(expected.getTime());

    expected = new Date(2021, 10, 26, 12);
    builder.addHour(12);
    expect(builder.toDate().getTime()).toBe(expected.getTime());
  });

  test('Difference Computations', () => {
    let alpha: Date = new Date(2021, 11, 10);
    let beta: Date = new Date(2021, 11, 9);

    let builder: TimeBuilder = new TimeBuilder(alpha).getDifference(beta);
    let expected: TimeBuilder = new TimeBuilder(0).addDay(1);
    expect(builder.asDifference()).toBe(expected.asDifference());
  });
});
