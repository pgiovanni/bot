import { Permission } from '#/bot';
import { GuildMember, User } from 'discord.js';
import { Config } from '@/service';
import { APIInteractionGuildMember, APIUser } from 'discord-api-types/v9';

export const PermissionIndex: Array<Permission> = [
  Permission.MUTED,
  Permission.BOT,
  Permission.USER,
  Permission.VALUED_CONTRIBUTOR,
  Permission.TRIAL_MODERATOR,
  Permission.MODERATOR,
  Permission.SENIOR_MODERATOR,
  Permission.ADMINISTRATOR,
  Permission.SENIOR_ADMINISTRATOR,
  Permission.HEAD_ADMINISTRATOR,
  Permission.DEVELOPER,
];

export function isUser(
  user: User | APIUser | GuildMember | APIInteractionGuildMember
): user is User | APIUser {
  return (
    (user as GuildMember | APIInteractionGuildMember).permissions === undefined
  );
}

export function calculatePermissions(
  actor: User | APIUser | GuildMember | APIInteractionGuildMember
) {
  let permissions = isUser(actor) ? '8' : actor.permissions;

  let user: User | APIUser;
  if (isUser(actor)) {
    user = actor;
  } else {
    user = actor.user;
  }

  let uid = user.id;
  let isBot = user.bot;

  const isAdmin =
    typeof permissions === 'string'
      ? (parseInt(permissions) & 0x8) === 0x8
      : permissions.has('ADMINISTRATOR');

  if (Config.DEVS.includes(uid)) {
    return Permission.DEVELOPER;
  } else {
    if (isBot) {
      return Permission.BOT;
    } else if (isAdmin) {
      return Permission.HEAD_ADMINISTRATOR;
    } else {
      return Permission.USER;
    }
  }
}

export function fromRole(role: string): Permission {
  role = role.toLowerCase().replace(/\s+/g, '_');
  if (role.includes('head_admin')) {
    return Permission.HEAD_ADMINISTRATOR;
  } else if (role.includes('senior_administrator')) {
    return Permission.SENIOR_ADMINISTRATOR;
  } else if (role.includes('administrator')) {
    return Permission.ADMINISTRATOR;
  } else if (role.includes('senior_moderator')) {
    return Permission.SENIOR_MODERATOR;
  } else if (role.includes('trial_moderator')) {
    return Permission.TRIAL_MODERATOR;
  } else if (role.includes('moderator')) {
    return Permission.MODERATOR;
  }
  return Permission.USER;
}

export function comparePermissions(alpha: Permission, beta: Permission) {
  return PermissionIndex.indexOf(alpha) >= PermissionIndex.indexOf(beta);
}
