import { Label, Log, logger } from '@/log';
import {
  CommandInteraction,
  Guild,
  GuildMember,
  InteractionReplyOptions,
  MessagePayload,
  Role,
  TextBasedChannel,
  ThreadChannel,
} from 'discord.js';

/**
 * Retrieve the Discord GuildMember from their API.
 * @param guild Guild ID
 * @param id Member ID
 * @returns GuildMember from the Discord API or null if not found
 */
export async function memberFromID(
  guild: Guild,
  id: string
): Promise<GuildMember | null> {
  let member: GuildMember | null = guild.members.cache.get(id) ?? null;
  if (!member) {
    member = await guild.members.fetch(id).catch(() => null);
    if (!member) {
      logger.error({
        message: `Failed to fetch member ${id} from guild ${guild.id}`,
        labels: Label(),
      });
    }
  }
  return member;
}

type MessageContent = string | MessagePayload | InteractionReplyOptions;
export async function reply(
  sender: CommandInteraction | TextBasedChannel,
  content: MessageContent,
  options?: 'followUp' | 'defer' | 'edit'
) {
  try {
    let contentChunks = [content];
    if (typeof content === 'string') {
      contentChunks = adjustContent(content);
    }

    if (sender instanceof CommandInteraction) {
      if (options === 'defer') {
        return await sender.deferReply();
      }

      if (options === 'followUp') {
        for (const chunk of contentChunks) {
          await sender.followUp(chunk);
        }
        return;
      }

      const head = contentChunks.shift();
      if (options === 'edit') {
        if (head) {
          await sender.editReply(head);
          return;
        }
      }

      if (head) {
        await sender.reply(head);
        for (const chunk of contentChunks) {
          await sender.followUp(chunk);
        }
        return;
      }
      logger.error({
        message: `Tried to send empty chunk! This is probably an error!`,
        labels: Label(),
      });
      return;
    }
    for (const chunk of contentChunks) {
      await sender.send(chunk);
    }
    return;
  } catch (err) {
    let logType: Log;
    if (sender instanceof CommandInteraction) logType = Log.COMMAND;
    else if (sender instanceof ThreadChannel) logType = Log.THREAD_CHANNEL;
    else logType = Log.CHANNEL;

    logger.error({
      message: `Failed to send '${JSON.stringify(
        content
      )}' to the Discord API!`,
      labels: Label({ type: logType, args: [sender] }),
    });
  }
}

function adjustContent(content: MessageContent): Array<MessageContent> {
  if (typeof content === 'string') {
    return adjustText(content);
  }
  logger.error({
    message: `Usage of unimplemented features (Content Length Adjustment)!`,
    labels: Label(),
  });
  return ['Unsupported content type!'];
}

/**
 * Adjust the content into chunks that can be sent to
 * the Discord API without violating the character limit.
 * @param content text message
 * @param limit character limit per message (default: 2000)
 */
function adjustText(content: string, limit?: number): Array<string> {
  if (!limit) limit = 2000;
  if (content.length < limit) return [content];

  let chunks: string[] = [''];
  let lines = content.split('\n').filter((line) => !!line);
  for (let line of lines) {
    if (line && line.length > limit) {
      const capacity = limit - (chunks[chunks.length - 1].length + 5);
      chunks[chunks.length - 1] += `\n${line.substring(0, capacity)} ...`;
      continue;
    }
    if (chunks[chunks.length - 1].length + line.length > limit - 1) {
      chunks.push('');
    }
    if (line) {
      chunks[chunks.length - 1] += `\n${line}`;
    }
  }
  return chunks;
}

export async function roleFromGuild(guild: Guild, role: string): Promise<Role> {
  let r = guild.roles.cache.find((r) => r.name === role);
  if (!r) {
    r = (await guild.roles.fetch(role))!;
  }
  return r;
}
