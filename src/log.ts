import {
  Channel,
  CommandInteraction,
  Guild,
  GuildMember,
  Message,
  PartialMessage,
  ThreadChannel,
  User,
} from 'discord.js';
import { createLogger, Logger } from 'winston';
import { default as LokiTransport } from 'winston-loki';
import { default as express } from 'express';
import { default as Prometheus } from 'prom-client';
import { default as DiscordTransport } from 'winston-discordjs';
import { Server } from 'http';

let logger: Logger = createLogger({
  transports: [
    new LokiTransport({
      level: 'debug',
      host: process.env.LOKI_URL!,
      labels: { job: process.env.BOT_HOST!, env: process.env.ENVIRONMENT! },
    }),
    new DiscordTransport({
      level: 'info',
    })
  ],
});

export enum Log {
  USER = 'user',
  GUILD = 'guild',
  MEMBER = 'member',
  CHANNEL = 'channel',
  THREAD_CHANNEL = 'thread_channel',
  MESSAGE = 'message',
  COMMAND = 'command',
  DATABASE = 'database',
}

type LabelParam = { type: Log; args?: any[] };
type LabelEntry = { [key: string]: string };
type LabelSet<T> = { [type: string]: T };
type StaticLabel = (...ctx: any[]) => LabelEntry;
type ComputedLabel = (...ctx: any[]) => Promise<LabelEntry>;

type RelevantMember = Pick<GuildMember, 'user' | 'guild' | 'displayAvatarURL'>;

// Labels
export const label: LabelSet<StaticLabel> = {};

label[Log.USER] = (user: User) => ({
  user_id: user.id,
  user_type: user.system ? 'system' : user.bot ? 'bot' : 'user',
});

label[Log.GUILD] = (guild: Guild) => ({
  guild_id: guild.id,
});

label[Log.MEMBER] = (member: RelevantMember) =>
  Object.assign(label[Log.USER](member.user), label[Log.GUILD](member.guild));

label[Log.CHANNEL] = (channel: Channel) => ({
  channel_id: channel.id,
  channel_type: channel.type,
});

label[Log.THREAD_CHANNEL] = (channel: ThreadChannel) => {
  const base = label[Log.CHANNEL](channel);
  return Object.assign(base, label[Log.GUILD](channel.guild));
};

label[Log.MESSAGE] = (message: Message | PartialMessage) => {
  const { author, member, channel, webhookId, id } = message;
  let core: { [key: string]: string } = assignUserLabels(
    {},
    author,
    member,
    webhookId
  );
  return Object.assign(core, label[Log.CHANNEL](channel), { message_id: id });
};

label[Log.COMMAND] = (command: CommandInteraction) => {
  let core: LabelEntry = {
    application_id: command.applicationId,
    command_id: command.commandId,
    ephemeral: `${command.ephemeral}`,
    deferred: `${command.deferred}`,
  };
  let member = command.member instanceof GuildMember ? command.member : null;
  core = assignUserLabels(core, command.user, member, null);
  core = Object.assign(core, label[Log.CHANNEL](command.channel));

  return core;
};

label[Log.DATABASE] = (database: string) => ({ database });

// Computed (Async) Labels
export const computedLabel: LabelSet<ComputedLabel> = {};

// Functions
function assignUserLabels(
  core: LabelEntry,
  author: User | null,
  member: RelevantMember | null,
  webhookId: string | null
): LabelEntry {
  if (member) {
    core = label[Log.MEMBER](member);
  } else if (author) {
    core = label[Log.USER](author);
  }
  if (webhookId) {
    core = Object.assign(core, { webhook_id: webhookId });
  }
  return core;
}

// Log Message Templates:
export const message = {};
message[Log.MEMBER] = (
  member: GuildMember,
  event: 'create' | 'update' | 'delete'
) => {
  return `Member '@${member.displayName}' was ${event}d in the server '${member.guild.name}'.`;
};

message[Log.MESSAGE] = (
  message: Message,
  event: 'create' | 'update' | 'delete'
) => {
  if ('name' in message.channel) {
    return `Message '${message.content}' was ${event}d in the channel '#${message.channel.name}'.`;
  } else {
    return `Message '${message.content}' was ${event}d by '@${message.author.tag}'.`;
  }
};

export const Label = (config?: LabelParam, ...labels: LabelEntry[]) => {
  let core: LabelEntry = {};
  if (config) {
    const coreFunc = label[config.type];
    core = config.args ? coreFunc(...config.args) : coreFunc();
  }

  if (labels && labels.length > 0) {
    return labels.reduce((acc, cur) => Object.assign(acc, cur), core);
  } else {
    return core;
  }
};

// Prometheus
export const MEMBER_RETENTION = new Prometheus.Gauge({
  name: 'member_retention',
  help: 'Member retention per Guild',
  labelNames: ['guild_id'],
});

export const MESSAGE_PRODUCTION = new Prometheus.Histogram({
  name: 'message_creation',
  help: 'Message production per Guild',
  labelNames: ['guild_id', 'channel_id', 'user_id'],
  buckets: [60, 60 * 10, 60 * 60].map((s) => s * 1000),
});

export const INTERACTION_USAGE = new Prometheus.Histogram({
  name: 'interaction_usage',
  help: 'Interaction usage per Guild',
  labelNames: ['guild_id', 'channel_id', 'user_id', 'command_id'],
  buckets: [60, 60 * 10, 60 * 60].map((s) => s * 1000),
});

// Prometheus Endpoint
let server: Server;
const init = () => {
  const app = express();
  const port = parseInt(process.env.METRICS_PORT ?? '9090');

  app.get('/metrics', async (req, res) => {
    try {
      res.set('Content-Type', Prometheus.register.contentType);
      res.end(await Prometheus.register.metrics());
    } catch (exc) {
      logger.error({
        message: `Failed to send Metrics!`,
      });
      res.status(500).end((<Error>exc).message);
    }
  });

  server = app.listen(port, () => {
    logger.info({
      message: `Metrics Endpoint available & listening on port ${port}`,
      labels: Label(),
    });
  });
};

export { logger, server, init as initPrometheus };
