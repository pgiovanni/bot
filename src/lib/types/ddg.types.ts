const API_URL = 'https://api.duckduckgo.com';
export { API_URL };

export interface SearchResult {
  Abstract: string;
  AbstractSource: string;
  AbstractText: string;
  AbstractURL: string;
  Answer: Answer | string;
  AnswerType: string;
  Definition: string;
  DefinitionSource: string;
  DefinitionURL: string;
  Entity: string;
  Heading: string;
  Image: string;
  ImageHeight: number;
  ImageIsLogo: number; //bool
  ImageWidth: number;
  Infobox: '' | Infobox;
  Redirect: string;
  RelatedTopics: Array<Result>;
  Results: Array<Result>;
  Type: string;
  // Non-important meta data for this application
  meta: any;
}

interface Answer {
  from: string;
  id: string;
  name: string;
  result: string;
  signal: string;
  templates: AnswerTemplates;
}

interface AnswerTemplates {
  group: string;
  options: { content: string };
}

interface Icon {
  Height: number | '';
  Width: number | '';
  URL: string;
}

export interface Result {
  FirstURL: string;
  Icon: Icon;
  Result: string;
  Text: string;
}

interface Infobox {
  content: Array<InfoboxContentEntry>;
  meta: Array<InfoboxMetaEntry>;
}

interface InfoboxContentEntry {
  data_type: string;
  label: string;
  sort_order: string;
  value: string;
  wiki_order: number;
}

interface InfoboxMetaEntry
  extends Omit<InfoboxContentEntry, 'sort_order' | 'wiki_order'> {}

/*
    Good Example Queries for Embed Implementation:
    - https://api.duckduckgo.com/?q=putin&format=json
    - https://api.duckduckgo.com/?q=Fall+Out+Boy&format=json
    - https://api.duckduckgo.com/?q=qwertz&format=json
    - https://api.duckduckgo.com/?q=epic%20games&format=json
    - https://api.duckduckgo.com/?q=uuid&format=json
 */
