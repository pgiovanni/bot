export class TimeBuilder {
  time: number;

  constructor(time?: number | Date) {
    if (typeof time === 'number') {
      this.time = time;
    } else {
      this.time = time === undefined ? 0 : time.getTime();
    }
  }

  // Operations
  getDifference(time: number | Date) {
    this.time -= typeof time === 'number' ? time : time.getTime();
    return this;
  }

  addSecond(amount: number) {
    this.time += amount * 1000;
    return this;
  }

  addMinute(amount: number) {
    this.addSecond(amount * 60);
    return this;
  }

  addHour(amount: number) {
    this.addMinute(amount * 60);
    return this;
  }

  addDay(amount: number) {
    this.addHour(24 * amount);
    return this;
  }

  addWeek(amount: number) {
    this.addDay(7 * amount);
    return this;
  }

  toDate(): Date {
    return new Date(this.time);
  }

  // Builder Outputs
  asDifference(): string {
    let time = this.time;
    const res: Array<number> = [];
    const units = ['ms', 's', 'm', 'h', 'd', 'y'];
    const conv = [1000, 60, 60, 24, 30, 12];

    for (let i = 0; i < conv.length; i++) {
      res.push(time % conv[i]);
      time = (time - res[res.length - 1]) / conv[i];
    }

    return res
      .filter((n) => !!n)
      .map((n, i) => `${n}${units[i]}`)
      .reverse()
      .join(' ');
  }
}
