import { Label, Log, logger } from '@/log';
import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, MessageEmbed } from 'discord.js';
import { default as google } from 'google-it';
import { reply } from '@/util/discord.util';

// 5 Requests per Minute
const RATE_LIMIT = (1000 * 60) / 5;

type GoogleResult = {
  title: string;
  link: string;
  snippet: string;
};

class GoogleCommand implements CommandExecutable {
  permission = Permission.USER;
  lastExecution: number = Date.now();

  async execute(interaction: CommandInteraction): Promise<void> {
    const wait = RATE_LIMIT - (Date.now() - this.lastExecution);
    if (wait > 0) {
      logger.debug({
        message: `Rate limit reached. Waiting ${wait}ms`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return reply(interaction, {
        content: `Please wait another ${Math.ceil(
          wait / 1000
        )} seconds before using this command.`,
      });
    }

    await reply(interaction, 'Searching...', 'defer');
    let query: string = interaction.options.getString('query', true);
    let results: Array<GoogleResult>;
    try {
      results = await google({
        'no-display': true,
        query: query,
        options: {
          qs: {
            q: encodeURIComponent(query),
            pws: 0,
            gl: 'US',
          },
        },
      });
    } catch (err) {
      logger.error({
        message: `Error while searching google\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: `An error occurred while searching google. Please try again later.`,
      });
    }

    const resultEmbeds = results.map((result) =>
      new MessageEmbed()
        .setTitle(result.title)
        .setDescription(result.snippet)
        .setURL(result.link)
        .setColor(
          result.title && result.snippet && result.link
            ? 'GREEN'
            : 'DARK_ORANGE'
        )
    );

    await interaction.editReply({
      content:
        'Feel free to filter the search results using a comma-separated 1-based number list ' +
        "or type 'keep' to keep all results in place.\n" +
        'Examples:\n```\n=> 3\n=> 1,3,7\n=> 1-3,6\n=> keep\n```',
      embeds: resultEmbeds,
    });

    if (interaction.channel) {
      let filter = await interaction.channel.awaitMessages({
        filter: (msg) => msg.author.id === interaction.user.id,
        maxProcessed: 1,
        time: 60 * 1000,
      });

      let response = filter.first();
      if (response) {
        if (response.content.toLowerCase() === 'keep') {
          await interaction.editReply({
            content: 'Results:',
          });
          await response.delete();
          logger.debug({
            message: `Keeping all ${results.length} results on the topic of '${query}'`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          return;
        }

        let rawIndices: Array<string> = response.content.split(/, */g);
        let indices: Array<number> = [];
        for (let rawIndex of rawIndices) {
          try {
            if (rawIndex.includes('-')) {
              let rawPair = rawIndex.split(/ *- */g);
              if (rawPair[0] === '') rawPair[0] = '1';
              if (rawPair[1] === '') rawPair[1] = '10';

              let pair = rawPair.map((n) => parseInt(n)).sort();
              if (pair.some((num) => isNaN(num))) {
                logger.debug({
                  message: `Invalid index pair '${rawIndex}'`,
                  labels: Label({ type: Log.COMMAND, args: [interaction] }),
                });
                await GoogleCommand.error(interaction, rawIndex, response);
                return;
              }
              for (let i = pair[0]; i <= pair[1]; i++) {
                if (i > 0 && i <= resultEmbeds.length) {
                  indices.push(i);
                }
              }
            } else {
              let parsedIndex = parseInt(rawIndex);
              if (isNaN(parsedIndex)) {
                logger.debug({
                  message: `Invalid index '${rawIndex}'`,
                  labels: Label({ type: Log.COMMAND, args: [interaction] }),
                });
                await GoogleCommand.error(interaction, rawIndex, response);
                return;
              }
              if (parsedIndex > 0 && parsedIndex <= resultEmbeds.length) {
                indices.push(parsedIndex);
              }
            }
          } catch (e) {
            logger.error({
              message: `Invalid index: ${rawIndex} not in range of ${results.length}`,
              labels: Label({ type: Log.COMMAND, args: [interaction] }),
            });
            await GoogleCommand.error(interaction, rawIndex, response);
            return;
          }
        }
        await response.delete();
        logger.debug({
          message: `Filtering ${indices.length} results on the topic of '${query}'`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        await interaction.editReply({
          content: 'Result:',
          embeds: indices.map((index) => resultEmbeds[index - 1]),
        });
        return;
      }
    }
  }

  private static async error(
    interaction: CommandInteraction,
    rawIndex: string,
    response
  ) {
    await reply(
      interaction,
      `Discarding all results. ||Invalid Index: ${rawIndex}||`,
      'followUp'
    );
    await response.delete();
    await interaction.deleteReply();
  }
}

const instance = new GoogleCommand();
export default instance;
