import { Label, Log, logger } from '@/log';
import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { reply } from '@/util/discord.util';

class LMDDGTFYCommand implements CommandExecutable {
  permission = Permission.USER;

  async execute(interaction: CommandInteraction): Promise<void> {
    const query = interaction.options.getString('query', true);
    logger.debug({
      message: 'Linking query of: ' + query,
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    return reply(interaction, {
      content: `https://lmddgtfy.net?q=${encodeURIComponent(query)}`,
    });
  }
}

const instance = new LMDDGTFYCommand();
export default instance;
