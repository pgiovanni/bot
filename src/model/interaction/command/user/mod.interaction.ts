import { Label, Log, logger } from '@/log';
import { CommandExecutable } from '@/lib';
import { Database } from '@/service';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { reply } from '@/util/discord.util';

class ModInteraction implements CommandExecutable {
  permission: Permission = Permission.HEAD_ADMINISTRATOR;

  async execute(interaction: CommandInteraction) {
    if (interaction.guild) {
      const role = interaction.options.getString('role', true);

      if (['DEVELOPER'].includes(role)) {
        return await reply(interaction, {
          content: "The 'Developer'-role is not assignable!",
          ephemeral: true,
        });
      }

      const gid: string = interaction.guildId!;
      const user = interaction.options.getUser('user', true);

      await Database.changeMemberPermission(gid, user.id, Permission[role]);
      logger.debug({
        message: 'Changed member permission',
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: 'Successfully applied changes.',
        ephemeral: true,
      });
    } else {
      logger.debug({
        message: 'Not in a guild.',
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await reply(interaction, 'No User Moderation available in this context.');
    }
  }
}

const instance = new ModInteraction();
export default instance;
