import {
  CommandInteraction,
  MessageEmbed,
  TextChannel,
  ThreadChannel,
} from 'discord.js';
import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { Label, Log, logger } from '@/log';
import * as fs from 'fs/promises';
import { reply } from '@/util/discord.util';

type CodeblockData = {
  [language: string]: {
    aliases: Array<string>;
    language: string;
    code: string;
  };
};
type CodeblockChannelMap = { [channelId: string]: string };

class CodeblocksCommand implements CommandExecutable {
  permission: Permission = Permission.USER;
  codeblockData: CodeblockData | undefined;
  codeblockChannelMap: CodeblockChannelMap | undefined;

  async loadData(): Promise<void> {
    try {
      this.codeblockData = JSON.parse(
        await fs.readFile('res/interactions/codeblock-data.json', 'utf-8')
      );
      this.codeblockChannelMap = JSON.parse(
        await fs.readFile(
          'res/interactions/codeblock-default-map.json',
          'utf-8'
        )
      );
    } catch (err) {
      logger.error({
        message: 'Failed to load codeblock data.\n' + err,
        labels: Label(),
      });
    }
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    if (
      !(
        interaction.channel instanceof TextChannel ||
        interaction.channel instanceof ThreadChannel
      )
    ) {
      await reply(interaction, 'This command is only available in a guild.');
      return;
    }

    if (!this.codeblockData || !this.codeblockChannelMap) {
      await this.loadData();
    }

    let lang: string | undefined;
    let code: string | undefined;

    let langOption = interaction.options.getString('lang', false);
    if (!langOption) {
      const option =
        this.codeblockChannelMap![interaction.channel.name] ?? 'typescript';
      lang = this.codeblockData![option].language;
      code = this.codeblockData![option].code;
    } else {
      langOption = langOption.toLowerCase();
      const aliases = Object.keys(this.codeblockData!).map(
        (key) => this.codeblockData![key]
      );
      for (const config of aliases) {
        if (
          config.language === langOption ||
          config.aliases.includes(langOption)
        ) {
          lang = config.language;
          code = config.code;
          break;
        }
      }

      if (!lang) {
        lang = this.codeblockData!['typescript'].language;
        code = this.codeblockData!['typescript'].code;
        logger.info({
          message: `Unknown programming language '${langOption}', please implement in res/`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
      }
    }

    logger.debug({
      message: 'Codeblocks command executed',
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    return reply(interaction, {
      embeds: [
        new MessageEmbed()
          .setTitle('Code Blocks')
          .setURL(
            'https://support.discord.com/hc/en-us/articles/210298617-Markdown-Text-101-Chat-Formatting-Bold-Italic-Underline-'
          )
          .setColor('BLURPLE')
          .setDescription(
            'Code Blocks in Discord are a better way of writing Code Snippets.\n\n' +
              'Using that feature code becomes more readable due to the Syntax Highlighting, which ' +
              'helps the reader understand the code better.\n\n' +
              'You can make use of codeblocks by writing:\n\n' +
              '\\`\\`\\`' +
              lang +
              '\n' +
              code +
              '\n' +
              '\\`\\`\\`\n\n' +
              'Where `' +
              lang +
              '` is the name of the language you want (markdown compatible).\n\n' +
              'This then becomes:\n' +
              '```' +
              lang +
              '\n' +
              code +
              '\n' +
              '```\n\n' +
              'Inline code can be used by surrounding text with \\`backticks\\` --> `backticks`.'
          ),
      ],
    });
  }
}

const instance = new CodeblocksCommand();
export default instance;
