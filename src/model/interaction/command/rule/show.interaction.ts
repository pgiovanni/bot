import { CommandExecutable } from '@/lib';
import { CommandInteraction, User } from 'discord.js';
import { comparePermissions } from '@/util/perm.util';
import { Label, Log, logger } from '@/log';
import { Permission } from '#/bot';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

const num = ['0️⃣', '1️⃣', '2️⃣', '3️⃣', '4️⃣', '5️⃣', '6️⃣', '7️⃣', '8️⃣', '9️⃣'];

class RuleShowCommand implements CommandExecutable {
  permission: Permission = Permission.USER;
  elevatedPermissions: Permission = Permission.TRIAL_MODERATOR;

  static async error(interaction: CommandInteraction, n: number) {
    await reply(interaction, {
      content: `❌ Failed to retrieve Rule #${n}!`,
      ephemeral: true,
    });
  }

  static write(n: number, text: string): string {
    const pref = `${n}`.replace(/\d/g, (m) => num[m.charCodeAt(0) - 48]);
    return `${pref} ${text}`;
  }

  static async handlePing(
    interaction: CommandInteraction,
    memberPing: User | null,
    ruleNumber: number | null,
    rules: string[]
  ) {
    let msg = '';
    if (memberPing) {
      msg += `<@${memberPing?.id}>:\n`;
    }
    if (ruleNumber) {
      msg += RuleShowCommand.write(ruleNumber, rules[ruleNumber - 1]);
    } else {
      msg += rules
        .map((rule, n) => RuleShowCommand.write(n + 1, rule))
        .join('\n');
    }
    logger.debug({
      message: `'@${interaction.user.tag}' successfully showed a rule!`,
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    await reply(interaction, msg);
    return;
  }

  async execute(interaction: CommandInteraction): Promise<void> {
    const gid = interaction.guildId ?? interaction.user.id;
    const guildName = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    let prismaUser = await Database.findOrCreateMember(
      interaction.user,
      interaction.guild
    );
    const ruleNumber = interaction.options.getInteger('number', false);
    const memberPing = interaction.options.getUser('user', false);
    const disallowPings =
      memberPing &&
      !comparePermissions(prismaUser.permission, this.elevatedPermissions);
    const guild = await Database.findOrCreateGuild(gid);

    if (disallowPings) {
      logger.debug({
        message: `User does not have proper permissions to ping others.`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: `Insufficient permissions to ping users with rules!`,
      });
    } else if (
      ruleNumber != null &&
      (ruleNumber < 1 || ruleNumber > guild.rules.length)
    ) {
      logger.error({
        message: `Failed to retrieve Rule #${ruleNumber} in guild '${guildName}'.`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await RuleShowCommand.error(interaction, ruleNumber);
      return;
    } else {
      await RuleShowCommand.handlePing(
        interaction,
        memberPing,
        ruleNumber,
        guild.rules
      );
      return;
    }
  }
}

const instance = new RuleShowCommand();
export default instance;
