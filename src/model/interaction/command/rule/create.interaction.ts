import { CommandExecutable } from '@/lib';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Permission } from '#/bot';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class RuleCreateCommand implements CommandExecutable {
  permission: Permission = Permission.SENIOR_ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    const guild = interaction.guildId ?? interaction.user.id;
    const guildName = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const text = interaction.options
      .getString('rule', true)
      .replace(/\\n/gi, '\n');

    try {
      await Database.addRule(guild, text);
      logger.debug({
        message: `Successfully added rule '${text}' to '${guildName}'.`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await reply(interaction, {
        content: '✅ Successfully added Rule!',
        ephemeral: true,
      });
    } catch (err) {
      logger.error({
        message: `The rule '${text}' could not be added to '${guildName}'\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await reply(interaction, {
        content: '❌ Failed to add Rule!',
        ephemeral: true,
      });
    }
  }
}

const instance = new RuleCreateCommand();
export default instance;
