import { CommandExecutable, TextBuilder } from '@/lib';
import { Ban, Permission } from '#/bot';
import { default as dayjs } from 'dayjs';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import BanService from '@/service/ban.service';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

const DATE_FORMAT: string = 'DD/MM/YYYY';

class BanListCommand implements CommandExecutable {
  permission: Permission = Permission.SENIOR_ADMINISTRATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    if (!interaction.guild) {
      return;
    }

    const guild = interaction.guild;
    const regex = interaction.options.getString('regex', false);
    const banDate = interaction.options.getString('ban-date', false);
    const pardonDate = interaction.options.getString('pardon-date', false);

    await BanService.invalidateBans();
    let bans = await Database.fetchBans(guild.id);

    // RegEx Filter
    if (regex) {
      let re: RegExp;
      try {
        re = new RegExp(regex);
      } catch (err) {
        logger.error({
          message: `The regex '${regex}' is invalid!`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        return reply(interaction, {
          content: `Error: Invalid RegEx Pattern (${regex})!`,
          ephemeral: true,
        });
      }
      bans = bans.filter((ban) => re.test(ban.reason + ' ' + ban.uid));
    }

    let filter = [
      {
        filter: banDate,
        date: (ban: Ban) => ban.createdAt,
      },
      {
        filter: pardonDate,
        date: (ban: Ban) => ban.pardonDate ?? dayjs().add(10, 'y').toDate(),
      },
    ];

    // Ban/Pardon-Time Filters
    for (let iter of filter) {
      if (iter.filter) {
        let times: Date | (Date | null)[] | null;
        try {
          times = parseTimeRange(iter.filter);
        } catch (err) {
          logger.error({
            message: `The time range ${iter.filter} is invalid!\n${err}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          return await reply(interaction, {
            content: `Error: Invalid time!`,
            ephemeral: true,
          });
        }

        if (times !== null) {
          if (times instanceof Array) {
            if (times[0] !== null) {
              let date = dayjs(times[0]).format('YYYY-MM-DD');
              bans = bans.filter((ban) =>
                dayjs(iter.date(ban)).isAfter(date, 'day')
              );
            }
            if (times[1] !== null) {
              let date = dayjs(times[1]).format('YYYY-MM-DD');
              bans = bans.filter((ban) =>
                dayjs(iter.date(ban)).isBefore(date, 'day')
              );
            }
          } else {
            let date = dayjs(<Date>times).format('YYYY-MM-DD');
            bans = bans.filter((ban) =>
              dayjs(iter.date(ban)).isSame(date, 'day')
            );
          }
        }
      }
    }

    if (bans.length > 0) {
      let content: TextBuilder = new TextBuilder();
      for (let ban of bans) {
        let bannedAt = dayjs(ban.createdAt);
        let pardonedAt = dayjs(ban.pardonDate);
        let frame = `${bannedAt.format(DATE_FORMAT)} - ${pardonedAt.format(
          DATE_FORMAT
        )}`;
        if (pardonedAt.diff(bannedAt, 'year') > 3) {
          frame = 'permanently';
        }
        content.addItemField('Reason', ban.reason ?? '*omitted*');
        content.addItemField(ban.uid, frame);
      }

      logger.debug({
        message: `Found & Filtered ${bans.length} bans in '${guild.name}'!`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: content.build(),
      });
    }

    logger.debug({
      message: `No bans found in '${guild.name}'!`,
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    return await reply(interaction, {
      content: 'There are no banned members in this guild!',
    });
  }
}

export function parseTimeRange(input: string): (Date | null)[] | Date | null {
  if (input.includes(' - ')) {
    let times: string[] = input.split(' - ');
    return times.map(parseTime);
  }
  return parseTime(input);
}

export function parseTime(input: string): Date | null {
  let re = /^(0[1-9]|[12]\d|3[01])\/(0[1-9]|1[0-2])\/(\d{4})$/;
  if (re.test(input)) {
    let date = dayjs(input, 'DD/MM/YYYY');
    if (date.isValid()) {
      return date.toDate();
    } else {
      throw `Invalid Date (${input}) provided!`;
    }
  }
  return null;
}

const instance = new BanListCommand();
export default instance;
