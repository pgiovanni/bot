import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class RoleDeleteCommand implements CommandExecutable {
  permission: Permission = Permission.MODERATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    let gid = interaction.guildId ?? interaction.user.id;
    let role = interaction.options.getRole('role', true);

    try {
      let guildRoles = await Database.fetchRoles(gid);
      if (guildRoles.length > 0) {
        let exists = guildRoles.find((found) => found.rid === role.id);
        if (exists) {
          try {
            await Database.deleteRole(exists.id);
            logger.debug({
              message: `Deleted (internal) role '${role.name}' from '${
                interaction.guild!.name
              }'.`,
              labels: Label({ type: Log.COMMAND, args: [interaction] }),
            });
            return await reply(interaction, {
              content: `Successfully unregistered <@&${role.id}>.`,
            });
          } catch (err) {
            logger.error({
              message: `Failed to delete role '${role.name}'.\n${err}`,
              labels: Label({ type: Log.COMMAND, args: [interaction] }),
            });
            return await reply(interaction, {
              content: '❌ Failed to delete Role!',
              ephemeral: true,
            });
          }
        }
      } else {
        logger.debug({
          message: `No registered roles found in '${interaction.guild!.name}'.`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        return await reply(interaction, {
          content: `This guild has no registered roles yet!`,
          ephemeral: true,
        });
      }
    } catch (err) {
      logger.error({
        message: `Failed to connect to database.\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: '❌ Failed to retrieve Guild Roles!',
        ephemeral: true,
      });
    }
  }
}

const instance = new RoleDeleteCommand();
export default instance;
