import { CommandExecutable, TextBuilder } from '@/lib';
import { CommandInteraction } from 'discord.js';
import { Permission } from '#/bot';
import { Database } from '@/service';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class RoleShowCommand implements CommandExecutable {
  permission: Permission = Permission.USER;

  async execute(interaction: CommandInteraction): Promise<void> {
    const gid = interaction.guildId ?? interaction.user.id;
    let all = interaction.options.getBoolean('non-assignable', false) ?? false;

    let roles = await Database.fetchRoles(gid);

    if (!all) {
      roles = roles.filter((role) => role.self);
    }

    if (roles.length === 0) {
      logger.debug({
        message: 'No roles found',
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      return await reply(interaction, {
        content: 'There are no registered roles on this server!',
        ephemeral: true,
      });
    }

    let textBuilder = new TextBuilder();
    roles.forEach((role, index) => {
      let roleName = `<@&${role.rid}>`;
      if (!role.self) {
        roleName += ' (not assignable)';
      }
      textBuilder.addEnumField(index + 1, roleName);
    });

    logger.debug({
      message: `Found ${roles.length} available roles`,
      labels: Label({ type: Log.COMMAND, args: [interaction] }),
    });
    return await reply(interaction, {
      content: textBuilder.build(),
    });
  }
}

const instance = new RoleShowCommand();
export default instance;
