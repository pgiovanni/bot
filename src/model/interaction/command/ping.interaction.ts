import { CommandInteraction } from 'discord.js';
import axios from 'axios';
import { URL } from '@/util/regex.util';
import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { Label, Log, logger } from '@/log';
import { reply } from '@/util/discord.util';

class PingCommand implements CommandExecutable {
  permission: Permission = Permission.USER;

  async execute(interaction: CommandInteraction): Promise<void> {
    const service = interaction.options.getString('service');

    if (service) {
      if (!URL.test(service)) {
        logger.debug({
          message: `Invalid URL: ${service}`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        return await reply(interaction, {
          ephemeral: true,
          content: 'Invalid URL! (Do not forget to include the protocol)',
        });
      }

      try {
        const res = await axios.get(service);
        const contentType = res.headers['content-type'].split(';')[0];

        if (res.status === 200) {
          if (contentType === 'text/html') {
            const title = (/<title>(.+)<\/title>/.exec(res.data) || [
              undefined,
              service,
            ])![1];
            await reply(interaction, `${title} is up!`);
          } else {
            await reply(interaction, `${service} is up!`);
          }
          logger.debug({
            message: `Successfully pinged ${service}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
        } else {
          logger.debug({
            message: `Failed to ping ${service} with response code ${res.status}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          await interaction.reply(
            `${service} returned response code: ${res.status}.`
          );
        }
      } catch (err) {
        logger.error({
          message: `Connection Error\n${err}`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
      }
    } else {
      await reply(interaction, 'Pong!');
      const latency = Date.now() - interaction.createdTimestamp;
      await reply(interaction, `Latency: ${latency}ms`, 'edit');
      logger.debug({
        message: `Pong! Discord API Latency: ${latency}ms`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
    }
  }
}

const instance = new PingCommand();
export default instance;
