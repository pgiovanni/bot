import { CommandExecutable } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, User } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class WarningAddCommand implements CommandExecutable {
  permission: Permission = Permission.TRIAL_MODERATOR;

  async execute(interaction: CommandInteraction): Promise<void> {
    const guild = interaction.guildId ?? interaction.user.id;
    const name = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const user: User = interaction.options.getUser('user', true);
    const reason: string = interaction.options
      .getString('reason', true)
      .replace(/\\n/gi, '\n');
    const context: string | null = interaction.options.getString(
      'context',
      false
    );

    const ctxRe = /(?:(\d+)@)?(\d+)\/(\d+)(?:@(\d+))?/gi;
    let ctxData: RegExpExecArray | null;
    let result: boolean;
    if (context && (ctxData = ctxRe.exec(context))) {
      let ctxChannel = ctxData[2];
      let ctxMessage = ctxData[3];
      let ctxAuthor = ctxData[1] ?? user.id;
      let ctxRev = !!ctxData[4] ? parseInt(ctxData![4]) : 1;

      result = await Database.createWarning(
        guild,
        user.id,
        reason,
        ctxChannel,
        ctxMessage,
        ctxAuthor,
        ctxRev
      );
    } else {
      result = await Database.createWarning(guild, user.id, reason);
    }

    if (result) {
      logger.debug({
        message: `Created warning for '${user.tag}' in '${name}' for '${reason}.`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await user.createDM().then((chan) =>
        chan.send({
          content:
            `You have been warned for following reason:\n` +
            reason +
            `\nPlease behave, otherwise there will be more serious consequences!`,
        })
      );
      await reply(interaction, {
        content: `✅ Successfully warned ${user.tag}!`,
      });
    } else {
      logger.error({
        message: `The warning could not be added to guild ${name}\n`,
        labels: Label(
          { type: Log.COMMAND, args: [interaction] },
          { target_user_id: user.id }
        ),
      });
      await reply(interaction, {
        content: '❌ Failed to add Warning!',
        ephemeral: true,
      });
    }
  }
}

const instance = new WarningAddCommand();
export default instance;
