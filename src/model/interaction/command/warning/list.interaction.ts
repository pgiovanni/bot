import { CommandExecutable, TextBuilder } from '@/lib';
import { Permission } from '#/bot';
import { CommandInteraction, MessageEmbed } from 'discord.js';
import { Label, Log, logger } from '@/log';
import { Database } from '@/service';
import { reply } from '@/util/discord.util';

class WarningListCommand implements CommandExecutable {
  permission: Permission = Permission.USER;

  async execute(interaction: CommandInteraction): Promise<void> {
    const guild = interaction.guildId ?? interaction.user.id;
    const name = interaction.guild
      ? interaction.guild.name
      : interaction.user.tag;
    const user = interaction.options.getUser('user', false) || interaction.user;

    try {
      const warnings = await Database.fetchWarnings(guild, user.id);

      if (warnings.length > 0) {
        let embed: MessageEmbed = new MessageEmbed()
          .setTitle(`Warnings`)
          .setColor(warnings.length > 5 ? 'DARK_RED' : 'DARK_ORANGE');
        let text: TextBuilder = new TextBuilder();
        for (let warning of warnings) {
          text.addItemField(`#${warning.wid}`, warning.reason);
        }
        embed.setDescription(text.build());
        logger.debug({
          message: `Fetched ${warnings.length} warnings for '${user.tag}' in '${name}' from the database.`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        await reply(interaction, {
          embeds: [embed],
        });
      } else {
        logger.debug({
          message: `No warnings found for '${user.tag}' in '${name}'.`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
        await reply(interaction, {
          content: `${user.tag} has not been warned *yet*!`,
        });
      }
    } catch (err) {
      logger.error({
        message: `The warnings for user '${user.tag}' could not be checked in guild '${name}'\n${err}`,
        labels: Label({ type: Log.COMMAND, args: [interaction] }),
      });
      await reply(interaction, {
        content: '❌ Failed to check Warnings!',
        ephemeral: true,
      });
    }
  }
}

const instance = new WarningListCommand();
export default instance;
