import { client } from '@/app';
import { Label, logger } from '@/log';
import {
  GuildMember,
  MessageEmbed,
  PartialGuildMember,
  TextChannel,
} from 'discord.js';
import { default as dayjs } from 'dayjs';
import { default as relativeTime } from 'dayjs/plugin/relativeTime';
import { default as parseFormat } from 'dayjs/plugin/customParseFormat';
import { TextBuilder } from '@/lib';
import { Database } from '.';

// Configure
dayjs.extend(relativeTime);
dayjs.extend(parseFormat);

const welcomeFooters: string[] = [
  "For sure won't be treated poorly",
  'This is good.',
  ':D',
  'Yes!',
  'YAY!',
  'What a wonderful creature',
];

class ChannelLogService {
  async log(msg: MessageEmbed, gid: string) {
    let logChannel = await Database.findLogChannel(gid)

    if(logChannel instanceof TextChannel) {
      return await logChannel!.send({
        embeds: [msg]
      });
    }
  }

  async memberJoin(member: GuildMember) {
    let createdAt = dayjs(member.user.createdAt);

    await this.log(
      new MessageEmbed()
        .setTitle('New Member')
        .setAuthor(ChannelLogService.getAuthor(member))
        .setDescription(
          new TextBuilder()
            .appendLine(`<@!${member.id}> (${member.user.tag})`)
            .addField('User ID', member.id)
            .addField(
              'Account Created',
              `${createdAt.fromNow()} (${createdAt.format(
                'ddd[, ]MMM[ ]D[, ]YYYY[, at ]h:mm:ss[ ]A'
              )})`
            )
            .build()
        )
        .setFooter({
          text: welcomeFooters[
            Math.floor(Math.random() * welcomeFooters.length)
          ],
        })
        .setTimestamp(),
      member.guild.id
    );
  }

  async memberRemove(member: GuildMember | PartialGuildMember) {
    let embed = new MessageEmbed();
    if (member.user) {
      embed
        .setTitle(`Member Left`)
        .setAuthor(ChannelLogService.getAuthor(<GuildMember>member))
        .setDescription(
          new TextBuilder()
            .addField(`<@!${member.user.id}>`, `(${member.user.tag})`)
            .addField('User ID', member.user.id)
            .build()
        );
    } else {
      embed
        .setTitle(`Member Deleted`)
        .setDescription(
          new TextBuilder()
            .addField(`User Tag`, `<@!${member.id}>`)
            .addField('User ID', member.id)
            .build()
        )
        .setTimestamp();
    }
    await this.log(embed, member.guild.id);
  }

  private static getAuthor(member: GuildMember) {
    return {
      name: member.nickname ?? member.user.username,
      iconURL:
        member.user.avatarURL({ dynamic: true, size: 512 }) ??
        member.user.defaultAvatarURL,
    };
  }
}

// @ts-ignore
const instance = new ChannelLogService();
// @ts-ignore
export default instance;
//@ts-ignore
