import * as schedule from 'node-schedule';
import { Label, logger } from '@/log';
import dayjs from 'dayjs';
import { Database } from '.';

class BanService {
  async init() {
    schedule.scheduleJob('0 */6 * * *', this.invalidateBans);
  }

  async invalidateBans() {
    let now = dayjs();
    const bans = await Database.fetchAllActiveBans();
    const invalidBans = bans.filter((ban) => now.isBefore(ban.pardonDate));
    for (let ban of invalidBans) {
      const { bid, uid, gid } = ban;
      await Database.revokeBan(gid, uid, bid);
    }
    logger.info({
      message: `Invalidated ${invalidBans.length} bans`,
      labels: Label(),
    });
  }
}

const instance = new BanService();
export default instance;
