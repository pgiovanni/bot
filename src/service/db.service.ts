import {
  AnyChannel,
  CommandInteraction,
  Guild,
  GuildMember, If,
  Message as DiscordMessage,
  User,
} from 'discord.js';
import { calculatePermissions, comparePermissions } from '@/util/perm.util';
import { APIUser } from 'discord-api-types/v9';

import { Label, Log, logger } from '@/log';
import {Message, Permission, PrismaClient as BotClient} from '#/bot';
import { PrismaClient as IndexClient } from '#/index';
import { client } from '@/app';

enum Database {
  BOT = 'bot',
  INDEX = 'index',
}

const BOT_DATABASE = new BotClient();
const INDEX_DATABASE = new IndexClient();

class DatabaseService {
  /**
   * Searches or creates a guild inside the database.
   * @param gid Guild ID
   * @param dm Whether the given chat is a real guild or a DM
   * @returns guild from database
   */
  async findOrCreateGuild(gid: string, dm?: boolean) {
    let guild = await BOT_DATABASE.guild.findUnique({
      where: { id: gid },
      include: { channels: true },
    });

    if (!guild) {
      /* Load Discord Guild and register name */
      let guildObj = client.guilds.cache.get(gid);
      if (!guildObj) {
        guildObj = await client.guilds.fetch(gid);
      }
      await INDEX_DATABASE.guild.create({
        data: {
          id: gid,
          name: guildObj.name,
          icon: guildObj.iconURL({ dynamic: true, size: 1024 }) ?? 'none',
        },
      });

      guild = await BOT_DATABASE.guild.create({
        data: { id: gid },
        include: { channels: true },
      });
      logger.debug({
        message: `Added ${
          !dm ? 'Guild' : '[Group] DM'
        } (${gid}) to the bot database.`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid }
        ),
      });
    }
    return guild;
  }

  /**
   * Finds or creates a log channel for a particular guild inside the database.
   * @param gid
   */

  async findLogChannel (gid: string) {

    let logChannel = BOT_DATABASE.guildChannel.findFirst({
      where: { id: gid, logChannel: true }
    })

    if(!logChannel) {
      return;
    }
    else {
      return logChannel;
    }
  }

  /**
   * Finds or creates a channel inside the database.
   * @param gid Guild ID
   * @param cid Channel ID
   * @returns channel from the database
   */

  async findOrCreateChannel(gid: string, cid: string) {
    let channel = await BOT_DATABASE.guildChannel.findUnique({
      where: { channelIdentifier: { gid: gid, id: cid } },
    });

    if (!channel) {
      let channelObj: AnyChannel | null | undefined =
        client.channels.cache.get(cid);
      if (!channelObj) {
        channelObj = await client.channels.fetch(cid);
      }
      if (channelObj && 'name' in channelObj) {
        await INDEX_DATABASE.channel.create({
          data: { id: cid, guildId: gid, name: channelObj.name },
        });
      }

      channel = await BOT_DATABASE.guildChannel.create({
        data: { id: cid, gid: gid },
      });
      logger.debug({
        message: `Added channel (${gid}/${cid}l).`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, channel_id: cid }
        ),
      });
    }
    return channel;
  }

  /**
   * Creates a user based on the provided Discord User & Guild inside the database.
   * @param user Discord user
   * @param guild Optional Guild Object
   * @returns member from the database
   */
  async findOrCreateMember(user: User | APIUser, guild: Guild | null) {
    const id = user.id;
    const gid = guild?.id ?? user.id;

    let member = await BOT_DATABASE.guildMember.findUnique({
      where: { memberIdentifier: { id: id, gid: gid } },
    });

    if (!member) {
      let avatar: string,
        tag: string = `${user.username}#${user.discriminator}`;
      if (guild) {
        let memberObj = guild.members.cache.get(id);
        if (!memberObj) {
          memberObj = await guild.members.fetch(id);
        }
        avatar = memberObj.user.displayAvatarURL({ dynamic: true, size: 1024 });
      } else {
        if (user.avatar) {
          let format = user.avatar.startsWith('a_') ? 'gif' : 'png';
          avatar = `https://cdn.discordapp.com/avatars/${id}/${user.avatar}.${format}?size=1024`;
        } else {
          avatar = `https://cdn.discordapp.com/embed/avatars/${
            parseInt(user.discriminator) % 5
          }.png`;
        }
      }
      await INDEX_DATABASE.user.create({ data: { id, tag, avatar } });

      let perm: Permission;
      if (guild) {
        try {
          const guildMember: GuildMember = await guild.members.fetch(id);
          perm = calculatePermissions(guildMember);
        } catch {
          perm = calculatePermissions(user);
        }
      } else {
        perm = calculatePermissions(user);
      }
      member = await BOT_DATABASE.guildMember.create({
        data: { id: id, gid: gid, permission: perm },
      });
      logger.debug({
        message: `Added member (${id}@${gid}).`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, user_id: id }
        ),
      });
    }
    return member;
  }

  async changeMemberPermission(
    gid: string,
    uid: string,
    permission: Permission
  ) {
    await BOT_DATABASE.guildMember.upsert({
      where: { memberIdentifier: { gid: gid, id: uid } },
      update: { permission },
      create: {
        id: uid,
        gid: gid,
        permission,
      },
    });
  }

  /**
   * Finds or creates a role inside the database.
   * @param gid Guild ID
   * @param rid Role ID
   * @param self Whether the role is self-assignable
   * @returns role from the database
   */
  async findOrCreateRole(gid: string, rid: string, self?: boolean) {
    let prismaRole = await BOT_DATABASE.guildRole.findFirst({
      where: { gid, rid },
    });

    if (!prismaRole || prismaRole.self !== !!self) {
      await BOT_DATABASE.guild.update({
        where: { id: gid },
        data: {
          roles: {
            connectOrCreate: {
              where: { roleIdentifier: { gid, rid } },
              create: { rid, self: !!self },
            },
          },
        },
      });
      logger.debug({
        message: `Upserted role to the database.`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, role_id: rid }
        ),
      });
      prismaRole = (await BOT_DATABASE.guildRole.findFirst({
        where: { gid, rid },
      }))!;
    }
    return prismaRole;
  }

  /**
   * Creates Messages inside the database.
   * @param message Discord message
   * @returns message from the database
   */
  async createMessage(message: DiscordMessage) {
    const member = await BOT_DATABASE.guildMember.findUnique({
      where: {
        memberIdentifier: {
          gid: message.guildId ?? message.author.id,
          id: message.author.id,
        },
      },
    });

    if (!(member && comparePermissions(member.permission, Permission.USER))) {
      return;
    }

    await INDEX_DATABASE.message.create({
      data: {
        id: message.id,
        channelId: message.channelId,
        userId: message.author.id,
        guildId: message.guildId ?? message.author.id,
        content: message.content,
      },
    });

    const msg: Message = await BOT_DATABASE.message.create({
      data: {
        id: message.id,
        gid: message.guildId ?? message.author.id,
        cid: message.channelId,
        uid: message.author.id,
        content: message.content,
      },
    });
    return msg;
  }

  /**
   * Assigns a role to a member in the database.
   * @param gid Guild ID
   * @param uid User ID
   * @param roleId Internal Role ID
   */
  async assignRole(gid: string, uid: string, roleId: number) {
    try {
      await BOT_DATABASE.guildMember.update({
        where: { memberIdentifier: { gid, id: uid } },
        data: { roles: { connect: { id: roleId } } },
      });
    } catch (err) {
      logger.error({
        message: `Failed to assign role (${gid}/${uid}/${roleId}).`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, user_id: uid }
        ),
      });
      return false;
    }
    return true;
  }

  async disconnectRole(gid: string, uid: string, roleId: number) {
    try {
      await BOT_DATABASE.guildMember.update({
        where: { memberIdentifier: { gid, id: uid } },
        data: { roles: { disconnect: { id: roleId } } },
      });
    } catch (err) {
      logger.error({
        message: `Failed to disconnect role (${gid}/${uid}/${roleId}).`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, user_id: uid }
        ),
      });
    }
  }

  /**
   * Fetch all guild roles from the database.
   * @param gid Guild ID
   * @returns guild roles
   */
  async fetchRoles(gid: string) {
    return await BOT_DATABASE.guild.findUnique({ where: { id: gid } }).roles();
  }

  /**
   * Deletes a role from the database.
   * @param rid Role ID
   */
  async deleteRole(rid: number) {
    await BOT_DATABASE.guildRole.delete({ where: { id: rid } });
  }

  async addRule(gid: string, rule: string) {
    await BOT_DATABASE.guild.update({
      where: { id: gid },
      data: { rules: { push: rule } },
    });
  }

  async setRules(gid: string, rules: string[]) {
    await BOT_DATABASE.guild.update({
      where: { id: gid },
      data: { rules: { set: rules } },
    });
  }

  async getWarningID(gid: string, uid: string) {
    let index: number =
      (
        await BOT_DATABASE.warning.aggregate({
          where: {
            gid: { equals: gid },
            AND: { uid: { equals: uid } },
          },
          _max: { wid: true },
        })
      )._max.wid || 0;
    return index + 1;
  }

  async createWarning(
    gid: string,
    uid: string,
    reason: string,
    ctxChannel?: string,
    ctxMessage?: string,
    ctxUser?: string,
    ctxRev?: number
  ) {
    try {
      let index = await this.getWarningID(gid, uid);
      let data = {
        wid: index,
        guild: { connect: { id: gid } },
        user: {
          connectOrCreate: {
            where: { memberIdentifier: { id: uid, gid: gid } },
            create: { id: uid, gid: gid },
          },
        },
        reason,
      };

      if (ctxChannel && ctxMessage) {
        data = Object.assign(data, {
          context: {
            connect: {
              messageIdentifier: {
                gid,
                cid: ctxChannel,
                uid: ctxUser ?? uid,
                id: ctxMessage,
                revision: ctxRev ?? 1,
              },
            },
          },
        });
      }

      await BOT_DATABASE.warning.create({ data });
      logger.debug({
        message: `Created warning (${gid}/${uid}/${index}).`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, user_id: uid }
        ),
      });
      return true;
    } catch (err) {
      logger.error({
        message: `Failed to create warning (${gid}/${uid}).\n${err}`,
        labels: Label(
          { type: Log.DATABASE, args: [Database.BOT] },
          { guild_id: gid, user_id: uid }
        ),
      });
      return false;
    }
  }

  async revokeWarning(gid: string, uid: string, wid: number) {
    await BOT_DATABASE.warning.update({
      where: { warningIdentifier: { gid, uid, wid } },
      data: { active: false },
    });
  }

  async fetchWarnings(gid: string, uid: string) {
    return BOT_DATABASE.warning.findMany({
      where: {
        gid: { equals: gid },
        uid: { equals: uid },
        active: { equals: true },
      },
    });
  }

  async createBan(gid: string, uid: string, reason: string, pardonDate: Date) {
    let bid: number =
      (
        await BOT_DATABASE.ban.aggregate({
          where: {
            gid: { equals: gid },
            AND: { uid: { equals: uid } },
          },
          _max: { bid: true },
        })
      )._max.bid || 0;
    bid++;

    await BOT_DATABASE.ban.create({
      data: { gid, uid, bid, pardonDate, reason },
    });
  }

  async revokeBans(gid: string, uid: string) {
    await BOT_DATABASE.ban.updateMany({
      where: {
        gid: gid,
        uid: uid,
      },
      data: {
        active: false,
      },
    });
  }

  async revokeBan(gid: string, uid: string, bid: number) {
    await BOT_DATABASE.ban.update({
      where: { banIdentifier: { gid, uid, bid } },
      data: { active: false },
    });
  }

  async fetchBans(gid: string) {
    return BOT_DATABASE.ban.findMany({
      where: {gid, active: true},
    });
  }

  async fetchAllActiveBans() {
    return BOT_DATABASE.ban.findMany({
      where: {active: true},
    });
  }

  async upsertCommand(command: CommandInteraction) {
    await INDEX_DATABASE.command.upsert({
      create: {
        id: command.commandId,
        name: command.commandName,
        subcommand: command.options.getSubcommand(false) ?? 'null',
        subcommandGroup: command.options.getSubcommandGroup(false) ?? 'null',
      },
      update: {
        name: command.commandName,
        subcommand: command.options.getSubcommand(false) ?? 'null',
        subcommandGroup: command.options.getSubcommandGroup(false) ?? 'null',
      },
      where: {
        id: command.commandId,
      },
    });
  }

  async setupLogChannel(gid: string, cid: string) {
    await this.findOrCreateChannel(gid, cid);

    // Reset all Log Channels
    await BOT_DATABASE.guild.update({
      where: { id: gid },
      data: {
        channels: {
          updateMany: {
            where: { gid: gid },
            data: { logChannel: false },
          },
        },
      },
    });
    // Set Proper Log-Channel
    await BOT_DATABASE.guild.update({
      where: { id: gid },
      data: {
        channels: {
          update: {
            where: { channelIdentifier: { gid: gid, id: cid } },
            data: { logChannel: true },
          },
        },
      },
    });
  }

  /**
   * Retrieve the number of messages from a user within a guild in the database.
   * @param gid Guild ID
   * @param uid User ID
   * @returns amount of messages the user has sent
   */
  async userMessageCount(gid: string, uid: string) {
    return BOT_DATABASE.message.count({
      where: {gid: gid, uid: uid},
    });
  }
}

const instance = new DatabaseService();
export default instance;
