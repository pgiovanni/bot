import { REST } from '@discordjs/rest';
import { Routes } from 'discord-api-types/v9';
import {
  Client,
  CommandInteraction,
  GuildMember,
  Interaction,
  Invite,
  Message,
  PartialMessage,
  TextChannel,
  ThreadChannel,
} from 'discord.js';
import {
  INTERACTION_USAGE,
  Label,
  Log,
  logger,
  MEMBER_RETENTION,
  MESSAGE_PRODUCTION,
  server,
  initPrometheus,
} from './log';
import * as fs from 'fs/promises';
import { interactions } from '@/model/interaction';
import { comparePermissions } from '@/util/perm.util';
import { Database, Config, BanService, ChannelLogService } from '@/service';
import { roleFromGuild } from './util/discord.util';
import {channel} from "diagnostics_channel";

let client: Client;
const Discord = require("discord.js")

/*
 * Configuration & Environment Setup
 */
async function init() {
  const env = process.env.ENVIRONMENT ?? 'develop';
  checkEnvironment();
  initPrometheus();

  await upsertInteractions(env);

  await BanService.init();
}

/*
 * Checks whether all required Environment
 * Variables are present.
 */
function checkEnvironment() {
  const required: string[] = ['TOKEN', 'CLIENT_ID'];
  const validEnv: boolean = required.every((v: string) => !!process.env[v]);

  if (!validEnv) {
    throw (
      'Environment not set up properly! Please check the README for ' +
      'Setup Instructions.'
    );
  }
}

/*
 * Send Interactions to the Discord API
 */
async function upsertInteractions(env: string) {
  if (!process.env.TOKEN || !process.env.CLIENT_ID) {
    throw 'Invalid Environment!';
  }

  const payload = JSON.parse(await fs.readFile('res/commands.json', 'utf-8'));
  const rest = new REST({ version: '9' }).setToken(process.env.TOKEN);
  logger.info({
    message: 'Uploading Interactions...',
    labels: Label(),
  });
  if (env === 'develop') {
    if (!process.env.DEV_GUILD) {
      throw "Development Environments have to supply a 'DEV_GUILD'.";
    }
    try {
      await rest.put(
        Routes.applicationGuildCommands(
          process.env.CLIENT_ID,
          process.env.DEV_GUILD
        ),
        { body: payload }
      );
    } catch (err) {
      logger.error({
        message: 'Failed to upload guild interactions.\n' + err,
        labels: Label(),
      });
    }
  } else {
    try {
      await rest.put(Routes.applicationCommands(process.env.CLIENT_ID), {
        body: payload,
      });
    } catch (err) {
      logger.error({
        message: 'Failed to upload global interactions.\n' + err,
        labels: Label(),
      });
    }
  }
  logger.info({
    message: 'Successfully updated interactions.',
    labels: Label(),
  });
}

function setup() {
  client = new Client({ intents: Config.INTENTS });

  client.on('ready', async () => {
    logger.info({
      message: `Logged in as ${client.user!.tag}!`,
      labels: Label(),
    });
    await Config.initialize(client);
  });

  client.on('debug', console.log);

  client.on('messageCreate', async (message: Message) => {
    const gid = message.guildId ?? message.author.id;
    const cid = message.channelId;

    MESSAGE_PRODUCTION.labels(gid, cid, message.author.id).observe(1);
    const channelName =
      'name' in message.channel ? message.channel.name : message.author.tag;
    logger.debug({
      message: `Received message from '@${message.author.tag}' in '#${channelName}'`,
      labels: Label({ type: Log.MESSAGE, args: [message] }),
    });

    await Database.findOrCreateGuild(gid, !message.guild);
    await Database.findOrCreateChannel(gid, cid);
    await Database.findOrCreateMember(message.author, message.guild);
    await Database.createMessage(message);
  });

  client.on(
    'messageUpdate',
    async (old: Message | PartialMessage, update: Message | PartialMessage) => {
      let authorTag = update.author?.tag ?? old.author?.tag ?? '<unknown>';
      let channelName =
        'name' in update.channel ? update.channel.name : authorTag;
      logger.debug({
        message: `Updated message from '${authorTag}' in '#${channelName}'`,
        labels: Label({ type: Log.MESSAGE, args: [update] }),
      });
      console.log(`author tag: ${authorTag} in channel: ${channelName}`)
    }
  );

  client.on('messageDelete', async (message: Message | PartialMessage) => {
    const gid = message.guildId ?? message.author?.id;
    if (!gid || message.webhookId || !(message.channel instanceof TextChannel)) {
      return;
    }

    const userName = message.author?.tag ?? '<unknown>';
    const channelName = message.channel.name;

    logger.debug({
      message: `Deleted message from '${userName}' in '#${channelName}'`,
      labels: Label({ type: Log.MESSAGE, args: [message] }),
    });



    let messageEmbed = new Discord.MessageEmbed.setDescription(message.content)
    messageEmbed.setAuthor(message.author?.username);
    messageEmbed.setTimestamp(message.createdTimestamp);

    await ChannelLogService.log(messageEmbed, <string>message.guildId);

  });

  client.on('threadCreate', async (channel: ThreadChannel) => {
    await channel.join();
    logger.debug({
      message: `Joined created thread channel ${channel.name}`,
      labels: Label({ type: Log.THREAD_CHANNEL, args: [channel] }),
    });
  });

  // TODO: ThreadUpdate

  client.on('interactionCreate', async (interaction: Interaction) => {
    if (interaction.isCommand()) {
      let found: any,
        subcommand = interaction.options.getSubcommand(false),
        subgroup = interaction.options.getSubcommandGroup(false);

      found = interactions[interaction.commandName];
      if (subgroup) {
        found = found[subgroup];
      }
      if (subcommand) {
        found = found[subcommand];
      }

      if (found) {
        const gid = interaction.guildId ?? interaction.user.id;
        const cid = interaction.channelId;

        await Database.findOrCreateGuild(gid, !interaction.guild);
        await Database.findOrCreateChannel(gid, cid);
        const user = await Database.findOrCreateMember(
          interaction.user,
          interaction.guild
        );

        await Database.upsertCommand(<CommandInteraction>interaction);

        if (comparePermissions(user.permission, found.permission)) {
          await found.execute(interaction);
          INTERACTION_USAGE.labels(
            interaction.guildId ?? interaction.user.id,
            interaction.channelId,
            interaction.user.id,
            interaction.commandId
          ).observe(1);
          logger.debug({
            message: `User ${interaction.user.tag} executed command ${interaction.commandName}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
        } else {
          logger.info({
            message: `User '${interaction.user.tag}' does not have the required permissions to execute ${interaction.commandName}`,
            labels: Label({ type: Log.COMMAND, args: [interaction] }),
          });
          return await interaction.reply({
            content: 'Insufficient Permissions. Cannot execute Command.',
            ephemeral: true,
          });
        }
      } else {
        logger.error({
          message:
            `Non-implemented interaction Received!\n` +
            `Name: ${interaction.commandName}, ` +
            `Subgroup: ${interaction.options.getSubcommandGroup(false)}, ` +
            `Sub Command: ${interaction.options.getSubcommand(false)};`,
          labels: Label({ type: Log.COMMAND, args: [interaction] }),
        });
      }
    }
    return undefined;
  });

  client.on('guildMemberAdd', async (member: GuildMember) => {
    await ChannelLogService.memberJoin(member);
    MEMBER_RETENTION.inc({ guild_id: member.guild.id });
    for (const roleID of Config.joinRoles[member.guild.id]) {
      let role = await roleFromGuild(member.guild, roleID);
      await member.roles.add(role);
    }

    logger.debug({
      message: `${member.user.tag} joined ${member.guild.name}`,
      labels: Label({ type: Log.MEMBER, args: [member] }),
    });
  });

  client.on('guildMemberRemove', async (member) => {
    await ChannelLogService.memberRemove(member);
    MEMBER_RETENTION.dec({ guild_id: member.guild.id });
    logger.debug({
      message: `${member.user.tag} left ${member.guild.name}`,
      labels: Label({ type: Log.USER, args: [member.user] }),
    });
  });

  client.on('inviteCreate', async (invite: Invite) => {
    let who: string = invite.inviter?.tag ?? 'Someone';
    logger.debug({
      message: `${who} created an invite with code '${invite.code}'.`,
      labels: Label({ type: Log.CHANNEL, args: [invite.channelId] }),
    });
  });

  client.on('inviteDelete', async (invite: Invite) => {
    let who: string = invite.inviter?.tag ?? 'Someone';
    logger.debug({
      message: `${who} deleted an invite with code '${invite.code}'.`,
      labels: Label({ type: Log.CHANNEL, args: [invite.channelId] }),
    });
  });

  client.login(process.env.TOKEN).then(() => {});
}



init().then(setup);

process.on('SIGTERM', () => {
  logger.info({
    message: 'Received SIGTERM, shutting down.',
    labels: Label(),
  });
  server.close();
  client.destroy();
});

export { client };
